#include <stdio.h>

void helloWorld()
{
    printf("hello world!");
}

int add(int a, int b)
{
    return a + b;
}

void printAdd(int add1, int add2)
{
    printf("%d + %d = %d", add1, add2, add(add1, add2));
}

int main()
{
    int ta = 2;
    int tb = 3;

    printAdd(ta, tb);

    return 0;
}
